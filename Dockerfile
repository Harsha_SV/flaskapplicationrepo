FROM ubuntu
RUN apt-get update -y && \
    apt-get install -y python3-pip python-dev

COPY ./req.txt /app/req.txt

WORKDIR /app

RUN pip install -r req.txt

COPY . /app

ENTRYPOINT [ "python" ]

CMD [ "app.py" ]